const principal = document.getElementById('principal')
const instrucoes = document.getElementById('instructions');
const play = document.getElementById('play');
const home = document.getElementById("home");
const game = document.getElementById('game')
const result1 = document.getElementById('result1')
const result2 = document.getElementById('result2')
const result3 = document.getElementById('result3')
const container = document.getElementById('container')

function random(min, max){

    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;     
}


home.onclick = function(){
    principal.classList.add("hidden")
    game.classList.add("hidden")
    result1.classList.add("hidden")
    result2.classList.add("hidden")
    result3.classList.add("hidden")

}

instrucoes.onclick = function(){
    principal.classList.remove("hidden");
    game.classList.add("hidden")
    result1.classList.add("hidden")
    result2.classList.add("hidden")
    result3.classList.add("hidden")

}

play.onclick = function(){
    principal.classList.add("hidden")
    game.classList.remove("hidden")
    result1.classList.add("hidden")
    result2.classList.add("hidden")
    result3.classList.add("hidden")

}

const rock = document.getElementById('rock')
const paper = document.getElementById('paper')
const scissors = document.getElementById('scissors')
const lizard = document.getElementById("lizard")
const spock = document.getElementById('spock')

const rock_img = '<img src="./img/rock.png"></img>';
const paper_img = '<img src="./img/paper.png"></img>';
const scissors_img = '<img src="./img/scissors.png"></img>';
const lizard_img = '<img src="./img/lizard.png"></img>';
const spock_img = '<img src="./img/spock.png"></img>';

rock.onclick = function(){
    let machine = random(1,5)
    game.classList.add('hidden')
    principal.classList.add("hidden")
    result1.classList.remove("hidden")
    result2.classList.remove("hidden")
    result3.classList.remove("hidden")
    result1.innerHTML = rock_img;

    if      (machine == 1){
        result2.innerHTML = rock_img
        result3.innerText = ''
        let resultado = document.createElement("h2")
        resultado.innerHTML='we had a tie'
        result3.appendChild(resultado)
    }
    else if (machine == 2){
        result2.innerHTML = paper_img
        result3.innerText = ''
        let resultado = document.createElement("h2")
        resultado.innerHTML='you lost'
        result3.appendChild(resultado)
    }
    else if (machine == 3){
        result2.innerHTML = scissors_img
        result3.innerText = ''
        let resultado = document.createElement("h2")
        resultado.innerHTML='you won'
        result3.appendChild(resultado)
    }
    else if (machine == 4){
        result2.innerHTML = lizard_img
        result3.innerText = ''
        let resultado = document.createElement("h2")
        resultado.innerHTML='you won'
        result3.appendChild(resultado)
    }
    else if (machine == 5){
        result2.innerHTML = spock_img
        result3.innerText = ''
        let resultado = document.createElement("h2")
        resultado.innerHTML='you lost'
        result3.appendChild(resultado)
    }

}


paper.onclick = function(){
    let machine = random(1,5)
    game.classList.add('hidden')
    principal.classList.add("hidden")
    result1.classList.remove("hidden")
    result2.classList.remove("hidden")
    result3.classList.remove("hidden")
    result1.innerHTML = paper_img;

    if      (machine == 1){
        result2.innerHTML = rock_img
        result3.innerText = ''
        let resultado = document.createElement("h2")
        resultado.innerHTML='you won'
        result3.appendChild(resultado)
    }
    else if (machine == 2){
        result2.innerHTML = paper_img
        result3.innerText = ''
        let resultado = document.createElement("h2")
        resultado.innerHTML='we had a tie'
        result3.appendChild(resultado)
    }
    else if (machine == 3){
        result2.innerHTML = scissors_img
        result3.innerText = ''
        let resultado = document.createElement("h2")
        resultado.innerHTML='you lost'
        result3.appendChild(resultado)
    }
    else if (machine == 4){
        result2.innerHTML = lizard_img
        result3.innerText = ''
        let resultado = document.createElement("h2")
        resultado.innerHTML='you lost'
        result3.appendChild(resultado)
    }
    else if (machine == 5){
        result2.innerHTML = spock_img
        result3.innerText = ''
        let resultado = document.createElement("h2")
        resultado.innerHTML='you won'
        result3.appendChild(resultado)
    }
}

scissors.onclick = function(){
    let machine = random(1,5)

    game.classList.add('hidden')
    principal.classList.add("hidden")
    result1.classList.remove("hidden")
    result2.classList.remove("hidden")
    result3.classList.remove("hidden")
    result1.innerHTML = scissors_img;

    if      (machine == 1){
        result2.innerHTML = rock_img
        result3.innerText = ''
        let resultado = document.createElement("h2")
        resultado.innerHTML='you lost'
        result3.appendChild(resultado)
    }
    else if (machine == 2){
        result2.innerHTML = paper_img
        result3.innerText = ''
        let resultado = document.createElement("h2")
        resultado.innerHTML='you won'
        result3.appendChild(resultado)
    }
    else if (machine == 3){
        result2.innerHTML = scissors_img
        result3.innerText = ''
        let resultado = document.createElement("h2")
        resultado.innerHTML='we had a tie'
        result3.appendChild(resultado)
    }
    else if (machine == 4){
        result2.innerHTML = lizard_img
        result3.innerText = ''
        let resultado = document.createElement("h2")
        resultado.innerHTML='you won'
        result3.appendChild(resultado)
    }
    else if (machine == 5){
        result2.innerHTML = spock_img
        result3.innerText = ''
        let resultado = document.createElement("h2")
        resultado.innerHTML='you lost'
        result3.appendChild(resultado)
    }
}

lizard.onclick = function(){
    let machine = random(1,5)

    game.classList.add('hidden')
    principal.classList.add("hidden")
    result1.classList.remove("hidden")
    result2.classList.remove("hidden")
    result3.classList.remove("hidden")
    result1.innerHTML = lizard_img;

    if      (machine == 1){
        result2.innerHTML = rock_img
        result3.innerText = ''
        let resultado = document.createElement("h2")
        resultado.innerHTML='you lost'
        result3.appendChild(resultado)
    }
    else if (machine == 2){
        result2.innerHTML = paper_img
        result3.innerText = ''
        let resultado = document.createElement("h2")
        resultado.innerHTML='you won'
        result3.appendChild(resultado)
    }
    else if (machine == 3){
        result2.innerHTML = scissors_img
        result3.innerText = ''
        let resultado = document.createElement("h2")
        resultado.innerHTML='you lost'
        result3.appendChild(resultado)
    }
    else if (machine == 4){
        result2.innerHTML = lizard_img
        result3.innerText = ''
        let resultado = document.createElement("h2")
        resultado.innerHTML='we had a tie'
        result3.appendChild(resultado)
    }
    else if (machine == 5){
        result2.innerHTML = spock_img
        result3.innerText = ''
        let resultado = document.createElement("h2")
        resultado.innerHTML='you won'
        result3.appendChild(resultado)
    }
}


spock.onclick = function(){
    let machine = random(1,5)

    game.classList.add('hidden')
    principal.classList.add("hidden")
    result1.classList.remove("hidden")
    result2.classList.remove("hidden")
    result3.classList.remove("hidden")
    result1.innerHTML = spock_img;

    if      (machine == 1){
        result2.innerHTML = rock_img
        result3.innerText = ''
        let resultado = document.createElement("h2")
        resultado.innerHTML='you won'
        result3.appendChild(resultado)
    }
    else if (machine == 2){
        result2.innerHTML = paper_img
        result3.innerText = ''
        let resultado = document.createElement("h2")
        resultado.innerHTML='you lost'
        result3.appendChild(resultado)
    }
    else if (machine == 3){
        result2.innerHTML = scissors_img
        result3.innerText = ''
        let resultado = document.createElement("h2")
        resultado.innerHTML='you won'
        result3.appendChild(resultado)
    }
    else if (machine == 4){
        result2.innerHTML = lizard_img
        result3.innerText = ''
        let resultado = document.createElement("h2")
        resultado.innerHTML='you lost'
        result3.appendChild(resultado)
    }
    else if (machine == 5){
        result2.innerHTML = spock_img
        result3.innerText = ''
        let resultado = document.createElement("h2")
        resultado.innerHTML='we had a tie'
        result3.appendChild(resultado)
    }


}




if (rock.onclick && machine == 1){

        let resultado = document.createElement("h2")
        resultado.innerHTML='we have a tie'
        result3.appendChild(resultado)
    
}
else if(paper.onclick && machine == 2){
    let resultado = document.createElement("h2")
    resultado.innerHTML='we have a tie'
    result3.appendChild(resultado)
}

else if(scissors.onclick && machine == 3){
    let resultado = document.createElement("h2")
    resultado.innerHTML='we have a tie'
    result3.appendChild(resultado)
}
else if(lizard.onclick   && machine == 4){
    let resultado = document.createElement("h2")
    resultado.innerHTML='we have a tie'
    result3.appendChild(resultado)
}
else if(spock.onclick    && machine == 5){
    let resultado = document.createElement("h2")
    resultado.innerHTML='we have a tie'
    result3.appendChild(resultado)
}


// function que 
//quando o usuario selecionar a imagem, 
//ele vai rodar o random, 
//vai devolver a imagem do user e da machine, nos result 1 e 2
//vai comparar os dois resultados 
// vai retornar uma frase no result 3




// user selecionar X, ele vai comparar com o resultado machine


// user rock e machine rock, empatou
// user rock e machine paper, voce perdeu
// user rock e machine scissors, voce ganhou
// user rock e machine lizard, voce ganhou
// user rock e machine spock, voce perdeu




// user paper e machine rock, voce ganhou
// user paper e machine paper, empatou
// user paper e machine scissors, voce perdeu
// user paper e machine lizard, voce perdeu
// user paper e machine spock, voce ganhou

// user scissors e machine rock, voce perdeu
// user scissors e machine paper, voce ganhou
// user scissors e machine scissors, empatou
// user scissors e machine lizard, voce ganhou
// user scissors e machine spock, voce perdeu

// user lizard e machine rock, voce perdeu
// user lizard e machine paper, voce ganhou
// user lizard e machine scissors, voce perdeu
// user lizard e machine lizard, empatou
// user lizard e machine spock, voce ganhou

// user spock e machine rock, voce ganhou
// user spock e machine paper, voce perdeu
// user spock e machine scissors, voce ganhou
// user spock e machine lizard, voce perdeu
// user spock e machine spock, empatou


